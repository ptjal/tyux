﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TyUx;

public class UxMenu : UxPanel 
{
    [Header("UI Reference")]
    public Button resumeButton;
    public Button quitButton;

    // Use this for initialization
    void Start() {
        resumeButton.onClick.AddListener(OnResumeClick);
        quitButton.onClick.AddListener(OnQuitClick);
        Display();
    }

    // Update is called once per frame
    void Update() {
        if (isActive && Input.GetButtonDown("Cancel")) {
            Destroy(gameObject);
        }
    }

    public void OnResumeClick() {
        Destroy(gameObject);
    }

    public void OnQuitClick() {
        Application.Quit();
    }

}

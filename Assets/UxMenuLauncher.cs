using UnityEngine;
using TyUx;

public class UxMenuLauncher : UxPanel {
    [Header("Prefabs")]
    public GameObject menuPrefab;

    // Update is called once per frame
    void Update() {
        // if we are not hidden (e.g.: paused) and pause key is pressed, launch pause menu
        if (!hidden && Input.GetButtonDown("Cancel"))
        {
            OnPause();
        }
    }

    public void OnPause() {
        // instantiate the pause panel prefab
        var panelGo = Instantiate(menuPrefab, GetComponentInParent<Canvas>().gameObject.transform);
        // setup a callback, so when the sub menu/panel is done, we display the current panel again
        var uxPanel = panelGo.GetComponent<UxPanel>();
        uxPanel.onDoneEvent.AddListener(Display);
        // now hide the current panel
        Hide();
    }

}